const { app,electron, BrowserWindow } = require('electron')
// var ipc = electron.ipcMain
const {ipcMain} = require('electron')


function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
win.loadFile('src/dashboard.html')

const client = new BrowserWindow({
  width: 800,
  height: 600,
  show:false,
  webPreferences: {
    nodeIntegration: true
  }
});
const contestant = new BrowserWindow({
  width: 800,
  height: 600,
  show:false,
  webPreferences: {
    nodeIntegration: true
  }
})

client.loadURL('file://'+__dirname+'/src/MainWindow/mainView.html')
client.on("close",()=>{
  // client.hide()
})
ipcMain.on('show-client',()=>{

  client.show()
  
})

contestant.loadURL('file://'+__dirname+'/src/ContestantManager/contestantView.html')
contestant.on("close",()=>{
  // client.hide()
})
ipcMain.on('show-contestant',()=>{
  contestant.show()
})

}
app.whenReady().then(createWindow)


